﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment1_MVC.Models;


namespace Assignment1_MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ViewResult Index()
        {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good morning" : "Good afternoon";
            return View("Sheridan" as object);
        }
        [HttpGet]
        public ViewResult RSVP()
        {
            return View();
        }
        [HttpPost]

        //have to change this method 
        public ViewResult RSVP(InviteResponse response)
        {
            if (ModelState.IsValid)
            {

                //we need to change the willAttend to QuestForm property but since I have no idea how to do the view part , I got no idea for this part as well
                if (response.WillAttend == true)
                {
                    //we need a add the SaveInvite method later like we did in class
                    SaveInvite(response);
                }
                return View("Thanks", response);
            }
            else
            {
                // there is a validation error
                return View();
            }

        }
        public ViewResult ShowLatestAttendees()
        {
            List<InviteResponse> acceptList = HttpContext.Cache["ACCEPT_LIST"] as List<InviteResponse>;

            return View("LatestAttendees", acceptList);
        }
        private void SaveInvite(InviteResponse response)
        {
            //check whether the information exists in the cache
            List<InviteResponse> acceptList = HttpContext.Cache["ACCEPT_LIST"] as List<InviteResponse>;

            //if the data does not exist in the cache, generate it and add it
            if (acceptList == null)
            {
                acceptList = new List<InviteResponse>();
                HttpContext.Cache["ACCEPT_LIST"] = acceptList;
            }

            //save the invite to the cache
            acceptList.Add(response);

        }
    }
}