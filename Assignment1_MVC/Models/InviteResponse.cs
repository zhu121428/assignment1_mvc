﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment1_MVC.Models
{
    public enum Technicallnterest
    {
        IoT,
        Cognitive,
        Wearable,
        AR_VR,
    }
    public class InviteResponse
    {
        /// <summary>
        /// Create a invites data model calss
        /// </summary>
        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please enter your TwtterAccount")]
        public string TwitterAccount { get; set; }

        [Required(ErrorMessage = "Please enter your TwtterAccount")]
        public Technicallnterest Interest { get; set; }

        [Required(ErrorMessage = "Please specify whether you'll attend")]
        public bool? WillAttend { get; set; }
    }
}